# USAGE
# python yolo.py --image images/baggage_claim.jpg --yolo yolo-coco

# import the necessary packages
from os import listdir
import numpy as np
import argparse
import time
import cv2
import os

# construct the argument parse and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-i", "--image", required=True,
# 	help="path to input image")
# ap.add_argument("-y", "--yolo", required=True,
# 	help="base path to YOLO directory")
# ap.add_argument("-c", "--confidence", type=float, default=0.5,
# 	help="minimum probability to filter weak detections")
# ap.add_argument("-t", "--threshold", type=float, default=0.3,
# 	help="threshold when applyong non-maxima suppression")
# args = vars(ap.parse_args())

def yolo_extract(img, yolo, confidence, threshold, num_i, folder):
    # load the COCO class labels our YOLO model was trained on
    labelsPath = os.path.sep.join([yolo, "coco.names"])
    LABELS = open(labelsPath).read().strip().split("\n")

    # initialize a list of colors to represent each possible class label
    np.random.seed(42)
    COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),
        dtype="uint8")

    # derive the paths to the YOLO weights and model configuration
    weightsPath = os.path.sep.join([yolo, "yolov3.weights"])
    configPath = os.path.sep.join([yolo, "yolov3.cfg"])

    # load our YOLO object detector trained on COCO dataset (80 classes)
    # print("[INFO] loading YOLO from disk...")
    net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)

    # load our input image and grab its spatial dimensions
    image = cv2.imread(img)
    (H, W) = image.shape[:2]

    # determine only the *output* layer names that we need from YOLO
    ln = net.getLayerNames()
    ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    # construct a blob from the input image and then perform a forward
    # pass of the YOLO object detector, giving us our bounding boxes and
    # associated probabilities
    blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
        swapRB=True, crop=False)
    net.setInput(blob)
    start = time.time()
    layerOutputs = net.forward(ln)
    end = time.time()

    # show timing information on YOLO
    # print("[INFO] YOLO took {:.6f} seconds".format(end - start))

    # initialize our lists of detected bounding boxes, confidences, and
    # class IDs, respectively
    boxes = []
    confidences = []
    classIDs = []
    # loop over each of the layer outputs
    for output in layerOutputs:
        # loop over each of the detections
        
        for detection in output:
            # extract the class ID and confidence (i.e., probability) of
            # the current object detection
            scores = detection[5:]
            classID = np.argmax(scores)
            confidence1 = scores[classID]

            # filter out weak predictions by ensuring the detected
            # probability is greater than the minimum probability
            if confidence1 > confidence:
                # scale the bounding box coordinates back relative to the
                # size of the image, keeping in mind that YOLO actually
                # returns the center (x, y)-coordinates of the bounding
                # box followed by the boxes' width and height
                box = detection[0:4] * np.array([W, H, W, H])
                (centerX, centerY, width, height) = box.astype("int")

                # use the center (x, y)-coordinates to derive the top and
                # and left corner of the bounding box
                x = int(centerX - (width / 2))
                y = int(centerY - (height / 2))

                # update our list of bounding box coordinates, confidences,
                # and class IDs
                boxes.append([x, y, int(width), int(height)])
                confidences.append(float(confidence1))
                classIDs.append(classID)
    # apply non-maxima suppression to suppress weak, overlapping bounding
    # boxes
    idxs = cv2.dnn.NMSBoxes(boxes, confidences, confidence,
        threshold)

    # ensure at least one detection exists
    if len(idxs) > 0:
        dict_of_dim = {'persons':{}, 'rackets': {}}
        person = 0
        tennis_racket = 0
        # loop over the indexes we are keeping
        for i in idxs.flatten():
            # extract the bounding box coordinates
            (x, y) = (boxes[i][0], boxes[i][1])
            (w, h) = (boxes[i][2], boxes[i][3])
            (imgH, imgW) = image.shape[:2]
            imgdim = [imgH , imgW]
            
            # draw a bounding box rectangle and label on the image
            # color = [int(c) for c in COLORS[classIDs[i]]]
            # cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
            # text = "{}: {:.4f}".format(LABELS[classIDs[i]], confidences[i])
            # cv2.putText(image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX,
            #     0.5, color, 2)
            
            if LABELS[classIDs[i]] == 'person':
                # path = r'C:\Users\Study\Desktop\Research\summer-research\FrameExtraction\Person'
                min_x = int(x)
                min_y = int(y)
                dict_of_dim.get('persons').update({'person{0}'.format(person): [min_x, min_y, w, h]})
                person+=1
                # (imgH, imgW) = cropped.shape[:2]
                # aspect_ratio = 300/imgH #resized
                # resized = cv2.resize(cropped, (int(imgW*aspect_ratio), 300), interpolation = cv2.INTER_AREA)
                # cv2.imwrite(os.path.join(path, 'person{0}.jpg'.format(i+5)), resized)
            if LABELS[classIDs[i]] == 'tennis racket':
                # path = r'C:\Users\Study\Desktop\Research\summer-research\FrameExtraction\Person'
                min_x = int(x)
                min_y = int(y)
                dict_of_dim.get('rackets').update({'racket{0}'.format(tennis_racket): [min_x, min_y, w, h]})
                tennis_racket+=1
        valid_boxes = group(dict_of_dim, imgdim)
        # print("# of people: ", len(valid_boxes))
        crop_combine_images(valid_boxes, image, num_i, folder)

def crop_combine_images(valid_boxes, image, num_i, folder):
    path = r'C:\Users\Study\Desktop\combined_photos'
    images = {}
    i = 0
    valid = True
    if len(valid_boxes) == 0:
        valid = False
    elif len(valid_boxes) < 5:
        dict_boxes = {'single': valid_boxes}
    else:
        dict_boxes = find_biggest(valid_boxes)
    if valid:
        for box in dict_boxes:
            if box == 'big' or box == 'single':
                min_x_crop = 40
                max_x_crop = 70
                max_y_crop = 50
            else:
                min_x_crop = 20
                max_x_crop = 30
                max_y_crop = 20
            if dict_boxes[box][0] < min_x_crop:
                min_x_crop = dict_boxes[box][0]
            if image.shape[1] - dict_boxes[box][0] + dict_boxes[box][3] < max_x_crop:
                max_x_crop = image.shape[1] - dict_boxes[box][0] + dict_boxes[box][3]
            if dict_boxes[box][1] < max_y_crop:
                max_y_crop = dict_boxes[box][1]
            cropped = image[int(dict_boxes[box][1] - max_y_crop): int(dict_boxes[box][1] + dict_boxes[box][3]), int(dict_boxes[box][0] - min_x_crop):int(dict_boxes[box][0] + dict_boxes[box][2] + max_x_crop)] #crops
            images.update({box: cropped})
            i+=4
        from combine_images import combine
        combined_image = combine(images)
        cv2.imwrite(os.path.join(path, 'id-{}.jpg'.format(num_i)), combined_image)

def find_biggest(valid_boxes):
    dict_boxes = {}
    if valid_boxes[2] * valid_boxes[3] > valid_boxes[6] * valid_boxes[7]:
        dict_boxes.update({'big': valid_boxes[0:4], "small" : valid_boxes[4:8]})
    else:
        dict_boxes.update({'small': valid_boxes[0:4], "big" : valid_boxes[4:8]})
    return dict_boxes

def checkperson(dict_person, midd):
    num_persons = len(dict_person.keys())
    biggie = 0
    personkey = ''
    valid_dim = {}
    if num_persons >= 2:
        for person  in dict_person:
            if dict_person[person][2] * dict_person[person][3] > biggie:
                biggie = dict_person[person][2] * dict_person[person][3]
                personkey = person
        
        if biggie > 4000:
            valid_dim.update({"big": dict_person[personkey]})
            dict_extra = dict_person.copy()
            del dict_extra[personkey]
            distance = 10000
            for person in dict_extra:
                if abs(midd - (dict_person[person][0] + (dict_person[person][2]) / 2))< distance:
                    distance =  midd - (dict_person[person][0] + (dict_person[person][2]) / 2)
                    personkey = person
            valid_dim.update({"small":dict_person[personkey]})
        else:
            distance = 10000
            for person in dict_person:
                if abs(midd - (dict_person[person][0] + (dict_person[person][2]) / 2))< distance:
                    distance =  midd - (dict_person[person][0] + (dict_person[person][2]) / 2)
                    personkey = person
            valid_dim.update({"single":dict_person[personkey]})
    else:
        for person in dict_person:
            valid_dim.update({"single":dict_person[person]})
    return valid_dim

def group(dict_of_dim, img_dim):
    mid = img_dim[1]/2
    num_rackets = len(dict_of_dim.get("rackets").keys())
    people_dim = checkperson(dict_of_dim['persons'], mid)
    valid_rackets = {}
    max_racket = ''
    max_width = 0
    if num_rackets == 2:
        for racket in dict_of_dim['rackets']:
            racket_dict = dict_of_dim['rackets']
            width = racket_dict[racket][2] 
            if width > max_width:
                max_racket = racket
                max_width = width
        valid_rackets.update({"big":dict_of_dim["rackets"][max_racket]})
        if max_racket == 'racket0':
            valid_rackets.update({"small":dict_of_dim["rackets"]['racket1']})
        else:
            valid_rackets.update({"small":dict_of_dim["rackets"]['racket0']})
    elif num_rackets == 1:

        valid_rackets.update({"single":dict_of_dim["rackets"]['racket0']})
    else:
        while len(dict_of_dim["rackets"]) >2:
            min = 99999
            min_racket = ''
            for racket in dict_of_dim["rackets"]:
                if dict_of_dim["rackets"][racket][2] * dict_of_dim["rackets"][racket][3] < min:
                    min_racket = racket
                    min = dict_of_dim["rackets"][racket][2] * dict_of_dim["rackets"][racket][3]
            del dict_of_dim["rackets"][min_racket]

    valid_boxes = match_person_racket(people_dim, valid_rackets, len(valid_rackets.keys()))
    return valid_boxes

def match_person_racket(people_dim, racket_dim, num_rackets):
    num_of_persons = len(people_dim.keys())
    minx = 0
    miny = 0
    w = 0
    h = 0
    valid_boxes = []
    if num_of_persons == 0:
        for key in racket_dim.keys():
            valid_boxes.extend([racket_dim[key][0], racket_dim[key][1], racket_dim[key][2], racket_dim[key][3]])
    elif num_rackets == 0:
        if len(people_dim.keys()) == 2:
            valid_boxes.extend([people_dim['big'][0], people_dim['big'][1], people_dim['big'][2], people_dim['big'][3]])
            valid_boxes.extend([people_dim['small'][0], people_dim['small'][1], people_dim['small'][2], people_dim['small'][3]])
        elif len(people_dim.keys()) == 1:
            valid_boxes.extend([people_dim['single'][0], people_dim['single'][1], people_dim['single'][2], people_dim['single'][3]])
    elif num_rackets == 1:
        if num_of_persons == 1:
            if people_dim['single'][2] *  people_dim['single'][3] > racket_dim['single'][2] * racket_dim['single'][3]:
                #width
                if people_dim['single'][0] < racket_dim['single'][0]:
                    minx = people_dim['single'][0]
                    if people_dim['single'][0] + people_dim['single'][2] > racket_dim['single'][0] + racket_dim['single'][2]:
                        w = people_dim['single'][2]
                    else:
                        w = racket_dim['single'][0]+racket_dim['single'][2] - minx
                else:
                    minx = racket_dim['single'][0]
                    if people_dim['single'][0] + people_dim['single'][2] < racket_dim['single'][0] + racket_dim['single'][2]:
                        w = racket_dim['single'][2]
                    else:
                        w = people_dim['single'][0]+ people_dim['single'][2] - minx
                #height
                if people_dim['single'][1] < racket_dim['single'][1]:
                    miny = people_dim['single'][1]
                    if people_dim['single'][1] + people_dim['single'][3] > racket_dim['single'][1] + racket_dim['single'][3]:
                        h = people_dim['single'][3]
                    else:
                        h = racket_dim['single'][1]+racket_dim['single'][3] - miny
                else:
                    miny = racket_dim['single'][1]
                    if people_dim['single'][1] + people_dim['single'][3] < racket_dim['single'][1] + racket_dim['single'][3]:
                        h = racket_dim['single'][3]
                    else:
                        h = people_dim['single'][1]+ people_dim['single'][3] - miny
            else:
                minx = people_dim['single'][0]
                miny = people_dim['single'][1]
                w = people_dim['single'][2]
                h = people_dim['single'][3]
            valid_boxes.extend([minx, miny, w, h])
        else:
            if people_dim['big'][2] *  people_dim['big'][3] > racket_dim['single'][2] * racket_dim['single'][3]:
                #width
                if people_dim['big'][0] < racket_dim['single'][0]:
                    minx = people_dim['big'][0]
                    if people_dim['big'][0] + people_dim['big'][2] > racket_dim['single'][0] + racket_dim['single'][2]:
                        w = people_dim['big'][2]
                    else:
                        w = racket_dim['single'][0]+racket_dim['single'][2] - minx
                else:
                    minx = racket_dim['single'][0]
                    if people_dim['big'][0] + people_dim['big'][2] < racket_dim['single'][0] + racket_dim['single'][2]:
                        w = racket_dim['single'][2]
                    else:
                        w = people_dim['big'][0]+ people_dim['big'][2] - minx
                #height
                if people_dim['big'][1] < racket_dim['single'][1]:
                    miny = people_dim['big'][1]
                    if people_dim['big'][1] + people_dim['big'][3] > racket_dim['single'][1] + racket_dim['single'][3]:
                        h = people_dim['big'][3]
                    else:
                        h = racket_dim['single'][1]+racket_dim['single'][3] - miny
                else:
                    miny = racket_dim['single'][1]
                    if people_dim['big'][1] + people_dim['big'][3] < racket_dim['single'][1] + racket_dim['single'][3]:
                        h = racket_dim['single'][3]
                    else:
                        h = people_dim['big'][1]+ people_dim['big'][3] - miny

                #add to valid boxes
                valid_boxes.extend([minx, miny, w, h])
                valid_boxes.extend([people_dim['small'][0], people_dim['small'][1], people_dim['small'][2], people_dim['small'][3]])

            elif people_dim['small'][2] *  people_dim['small'][3] > racket_dim['single'][2] * racket_dim['single'][3]:
                #width
                if people_dim['small'][0] < racket_dim['single'][0]:
                    minx = people_dim['small'][0]
                    if people_dim['small'][0] + people_dim['small'][2] > racket_dim['single'][0] + racket_dim['single'][2]:
                        w = people_dim['small'][2]
                    else:
                        w = racket_dim['single'][0]+racket_dim['single'][2] - minx
                else:
                    minx = racket_dim['single'][0]
                    if people_dim['small'][0] + people_dim['small'][2] < racket_dim['single'][0] + racket_dim['single'][2]:
                        w = racket_dim['single'][2]
                    else:
                        w = people_dim['small'][0]+ people_dim['small'][2] - minx
                #height
                if people_dim['small'][1] < racket_dim['single'][1]:
                    miny = people_dim['small'][1]
                    if people_dim['small'][1] + people_dim['small'][3] > racket_dim['single'][1] + racket_dim['single'][3]:
                        h = people_dim['small'][3]
                    else:
                        h = racket_dim['single'][1]+racket_dim['single'][3] - miny
                else:
                    miny = racket_dim['single'][1]
                    if people_dim['small'][1] + people_dim['small'][3] < racket_dim['single'][1] + racket_dim['single'][3]:
                        h = racket_dim['single'][3]
                    else:
                        h = people_dim['small'][1]+ people_dim['small'][3] - miny
                
                #add to valid boxes
                valid_boxes.extend([minx, miny, w, h])
                valid_boxes.extend([people_dim['big'][0], people_dim['big'][1], people_dim['big'][2], people_dim['big'][3]])
            else:
                #add to valid boxes
                valid_boxes.extend([people_dim['big'][0], people_dim['big'][1], people_dim['big'][2], people_dim['big'][3]])
                valid_boxes.extend([people_dim['small'][0], people_dim['small'][1], people_dim['small'][2], people_dim['small'][3]])

    #two rackets

    else:
        if len(people_dim.keys()) == 1:
            if people_dim['single'][2] *  people_dim['single'][3] > racket_dim['big'][2] * racket_dim['big'][3]:
                #width
                if people_dim['single'][0] < racket_dim['big'][0]:
                    minx = people_dim['single'][0]
                    if people_dim['single'][0] + people_dim['single'][2] > racket_dim['big'][0] + racket_dim['big'][2]:
                        w = people_dim['single'][2]
                    else:
                        w = racket_dim['big'][0]+racket_dim['big'][2] - minx
                else:
                    minx = racket_dim['big'][0]
                    if people_dim['single'][0] + people_dim['single'][2] < racket_dim['big'][0] + racket_dim['big'][2]:
                        w = racket_dim['big'][2]
                    else:
                        w = people_dim['single'][0]+ people_dim['single'][2] - minx
                #height
                if people_dim['single'][1] < racket_dim['big'][1]:
                    miny = people_dim['single'][1]
                    if people_dim['single'][1] + people_dim['single'][3] > racket_dim['big'][1] + racket_dim['big'][3]:
                        h = people_dim['single'][3]
                    else:
                        h = racket_dim['big'][1]+racket_dim['big'][3] - miny
                else:
                    miny = racket_dim['big'][1]
                    if people_dim['single'][1] + people_dim['single'][3] < racket_dim['big'][1] + racket_dim['big'][3]:
                        h = racket_dim['big'][3]
                    else:
                        h = people_dim['single'][1]+ people_dim['single'][3] - miny
            else:
                minx = people_dim['single'][0]
                miny = people_dim['single'][1]
                w = people_dim['single'][2]
                h = people_dim['single'][3]
            valid_boxes.extend([minx, miny, w, h])
        elif people_dim['big'][2] *  people_dim['big'][3] > racket_dim['big'][2] * racket_dim['big'][3]:
            #big person
            #width
            if people_dim['big'][0] < racket_dim['big'][0]:
                minx = people_dim['big'][0]
                if people_dim['big'][0] + people_dim['big'][2] > racket_dim['big'][0] + racket_dim['big'][2]:
                    w = people_dim['big'][2]
                else:
                    w = racket_dim['big'][0]+racket_dim['big'][2] - minx
            else:
                minx = racket_dim['big'][0]
                if people_dim['big'][0] + people_dim['big'][2] < racket_dim['big'][0] + racket_dim['big'][2]:
                    w = racket_dim['big'][2]
                else:
                    w = people_dim['big'][0]+ people_dim['big'][2] - minx
            #height
            if people_dim['big'][1] < racket_dim['big'][1]:
                miny = people_dim['big'][1]
                if people_dim['big'][1] + people_dim['big'][3] > racket_dim['big'][1] + racket_dim['big'][3]:
                    h = people_dim['big'][2]
                else:
                    h = racket_dim['big'][1]+racket_dim['big'][3] - miny
            else:
                miny = racket_dim['big'][1]
                if people_dim['big'][1] + people_dim['big'][3] < racket_dim['big'][1] + racket_dim['big'][3]:
                    h = racket_dim['big'][3]
                else:
                    h = people_dim['big'][1]+ people_dim['big'][3] - miny
            valid_boxes.extend([minx, miny, w, h])
            #small person
            if people_dim['small'][2] *  people_dim['small'][3] > racket_dim['small'][2] * racket_dim['small'][3]:
                if people_dim['small'][0] < racket_dim['small'][0]:
                    minx = people_dim['small'][0]
                    if people_dim['small'][0] + people_dim['small'][2] > racket_dim['small'][0] + racket_dim['small'][2]:
                        w = people_dim['small'][2]
                    else:
                        w = racket_dim['small'][0]+racket_dim['small'][2] - minx
                else:
                    minx = racket_dim['small'][0]
                    if people_dim['small'][0] + people_dim['small'][2] < racket_dim['small'][0] + racket_dim['small'][2]:
                        w = racket_dim['small'][2]
                    else:
                        w = people_dim['small'][0]+ people_dim['small'][2] - minx
                #height
                if people_dim['small'][1] < racket_dim['small'][1]:
                    miny = people_dim['small'][1]
                    if people_dim['small'][1] + people_dim['small'][3] > racket_dim['small'][1] + racket_dim['small'][3]:
                        h = people_dim['small'][2]
                    else:
                        h = racket_dim['small'][1]+racket_dim['small'][3] - miny
                else:
                    miny = racket_dim['small'][1]
                    if people_dim['small'][1] + people_dim['small'][3] < racket_dim['small'][1] + racket_dim['small'][3]:
                        h = racket_dim['small'][3]
                    else:
                        h = people_dim['small'][1]+ people_dim['small'][3] - miny
                valid_boxes.extend([minx, miny, w, h])
            else:
                valid_boxes.extend([people_dim['small'][0], people_dim['small'][1], people_dim['small'][2], people_dim['small'][3]])
        else:
            valid_boxes.extend([people_dim['big'][0], people_dim['big'][1], people_dim['big'][2], people_dim['big'][3]])
            valid_boxes.extend([people_dim['small'][0], people_dim['small'][1], people_dim['small'][2], people_dim['small'][3]])
    return valid_boxes