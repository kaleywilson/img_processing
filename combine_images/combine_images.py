import cv2
import numpy as np

def combine(images):
    if len(images.keys()) == 2:
        img1 = images['big']
        img2 = images['small']
        
        height = img1.shape[0] /3
        aspect_ratio = height/img2.shape[0] #resized
        img2 = cv2.resize(img2, (int(aspect_ratio*img2.shape[1]), int(height)), interpolation = cv2.INTER_CUBIC)
        h1, w1 = img1.shape[:2]
        h2, w2 = img2.shape[:2]

        img3 = np.zeros((max(h1, h2), w1+w2, 3), np.uint8)
        img3[:h1, :w1, :3] = img1
        img3[:h2, w1:w1+w2, :3] = img2

        img3 = cv2.resize(img3, (int(40), int(50)))
        return img3
    else:
        img4 = images["single"]
        img4 = cv2.resize(img4, (40, 50))
        return img4

