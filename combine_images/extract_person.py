#this class will extract images and place them in another folder
from os import listdir
import numpy as np
import argparse
import time
import cv2
import os
from yolo_extract import yolo_extract

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
ap.add_argument("-y", "--yolo", required=True,
	help="base path to YOLO directory")
ap.add_argument("-c", "--confidence", type=float, default=0.2,
	help="minimum probability to filter weak detections")
ap.add_argument("-t", "--threshold", type=float, default=0.3,
	help="threshold when applyong non-maxima suppression")
args = vars(ap.parse_args())

i = 0
folder_count = 0
for folder in os.listdir(args["image"]):
	for img in os.listdir(os.path.join(args["image"], folder)):
		print(img)
		yolo_extract(os.path.join(args["image"], folder, img), args['yolo'], args['confidence'], args['threshold'], i, folder)
		i+=1
		folder_count +=1
	print("{} Count: {}".format(folder, folder_count))
	folder_count = 0	
	
