# USAGE
# python yolo.py --image images/baggage_claim.jpg --yolo yolo-coco

# import the necessary packages
from os import listdir
import numpy as np
import argparse
import time
import cv2
import os
   
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
ap.add_argument("-y", "--yolo", required=True,
	help="base path to YOLO directory")
ap.add_argument("-c", "--confidence", type=float, default=0.2,
	help="minimum probability to filter weak detections")
ap.add_argument("-t", "--threshold", type=float, default=0.3,
	help="threshold when applyong non-maxima suppression")
args = vars(ap.parse_args())

# load the COCO class labels our YOLO model was trained on
labelsPath = os.path.sep.join([args["yolo"], "coco.names"])
LABELS = open(labelsPath).read().strip().split("\n")
data_path = r"C:\Users\Study\Desktop\datta"


# initialize a list of colors to represent each possible class label
np.random.seed(42)
COLORS = np.random.randint(0, 255, size=(len(LABELS), 3),
	dtype="uint8")

# derive the paths to the YOLO weights and model configuration
weightsPath = os.path.sep.join([args["yolo"], "yolov3.weights"])
configPath = os.path.sep.join([args["yolo"], "yolov3.cfg"])

# load our YOLO object detector trained on COCO dataset (80 classes)
print("[INFO] loading YOLO from disk...")
net = cv2.dnn.readNetFromDarknet(configPath, weightsPath)

labels_dict = {}
count = 0
folder_path = args["image"]
for folder in os.listdir(folder_path):
    for img in os.listdir(os.path.join(folder_path, folder)):
        # load our input image and grab its spatial dimensions
        image = cv2.imread(os.path.join(folder_path, folder, img))
        (H, W) = image.shape[:2]
        mid_x = W/2
        # determine only the *output* layer names that we need from YOLO
        ln = net.getLayerNames()
        ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]

        # construct a blob from the input image and then perform a forward
        # pass of the YOLO object detector, giving us our bounding boxes and
        # associated probabilities
        blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
            swapRB=True, crop=False)
        net.setInput(blob)
        start = time.time()
        layerOutputs = net.forward(ln)
        end = time.time()

        # show timing information on YOLO
        print("[INFO] YOLO took {:.6f} seconds".format(end - start))

        # initialize our lists of detected bounding boxes, confidences, and
        # class IDs, respectively
        boxes = []
        confidences = []
        classIDs = []
        # loop over each of the layer outputs
        for output in layerOutputs:
            # loop over each of the detections
            
            for detection in output:
                # extract the class ID and confidence (i.e., probability) of
                # the current object detection
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]

                # filter out weak predictions by ensuring the detected
                # probability is greater than the minimum probability
                if confidence > args["confidence"]:
                    # scale the bounding box coordinates back relative to the
                    # size of the image, keeping in mind that YOLO actually
                    # returns the center (x, y)-coordinates of the bounding
                    # box followed by the boxes' width and height
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")

                    # use the center (x, y)-coordinates to derive the top and
                    # and left corner of the bounding box
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    # update our list of bounding box coordinates, confidences,
                    # and class IDs
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)
            

        # apply non-maxima suppression to suppress weak, overlapping bounding
        # boxes
        idxs = cv2.dnn.NMSBoxes(boxes, confidences, args["confidence"],
            args["threshold"])


        def find_objects(idxs, classIDs, mid_x, path):
            global count
            rackets_num = 0
            rackets = []
            person = []
            data = []
            for i in idxs:
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])

                if LABELS[classIDs[i]] == 'person':
                    person.extend([x, y, w, h])
                elif LABELS[classIDs[i]] == 'tennis racket':
                    rackets.extend([x, y, w, h])
                    rackets_num +=1

            #decide to use frame
            use = True
            if rackets_num > 2:
                use = False
            
            if use:

                #ADD PERSONS TO DATA
                max_area = 0
                close = -1
                i = 0
                if len(person) == 0:
                    data.extend([-1, -1, -1, -1, -1, -1, -1, -1])
                else:
                    while i < len(person):
                        area = person[i+2]*person[i+3]
                        if area > max_area:
                            max_area = area
                            close = i
                        i+=4
                    if max_area > 4000:
                        data.extend([person[close], person[close+1], person[close+2], person[close+3]])
                        distance = 100000
                        middle = -1
                        i = 0
                        while i < len(person):
                            if i != close:
                                if abs(mid_x - person[i]) < distance:
                                    distance =  mid_x - person[i]
                                    middle = i
                            i+=4
                        data.extend([person[middle], person[middle+1], person[middle+2], person[middle+3]])
                    else:
                        distance = 100000
                        middle = -1
                        i = 0
                        while i < len(person):
                            if abs(mid_x - person[i]) < distance:
                                distance =  abs(mid_x - person[i])
                                middle = i
                            i+=4
                        data.extend([person[middle], person[middle+1], person[middle+2], person[middle+3]])
                        data.extend([-1, -1, -1, -1])
                
                #ADD RACKETS TO DATA
                data.append(rackets_num)
                data.extend(rackets)
                if rackets_num == 1:
                    data.extend([-1, -1, -1, -1])
                elif rackets_num == 0:
                    data.extend([-1, -1, -1, -1, -1, -1, -1, -1])

                idimg = "{}-{}".format('id', count)
                np.save(os.path.join(path, idimg), np.array(data))

                if folder == "BackhandB":
                    labels_dict.update({idimg: 0})
                elif folder == "BackhandV":
                    labels_dict.update({idimg: 1})
                elif folder == "ForehandB":
                    labels_dict.update({idimg: 2})
                elif folder == "ForehandV":
                    labels_dict.update({idimg: 3})
                elif folder == "OverheadV":
                    labels_dict.update({idimg: 4})
                elif folder == "Serve":
                    labels_dict.update({idimg: 5})
                elif folder == "Dead":
                    labels_dict.update({idimg: 6})
                count+=1           

        # ensure at least one detection exists
        if len(idxs) > 0:
            find_objects(idxs.flatten(), classIDs, mid_x, data_path)

        print("# of labels: ", len(labels_dict.keys()))
        print("Count: ", count)

np.save(os.path.join(data_path, "labels_final"), labels_dict)