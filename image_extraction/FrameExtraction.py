import cv2 as cv
import os
import numpy as np

video = cv.VideoCapture('tennismatch4.mp4')
strokes = {'Dead': [], 'ForehandV' : [], 'ForehandB' : [], 'BackhandB' : [], 'BackhandV' : [], 'Serve' : [], 'OverheadV' : []}

dead = False
f_v = False
f_b = False
b_b = False
b_v = False
s = False
o = False
fast = False
pause = False
remove = False

def saveImage(letter):
    global dead, f_v, f_b, b_b, b_v, s, fast, pause, o, remove
    if letter == ord('f'):
        f_b = True
        dead, f_v, b_b, b_v, s, o = False, False, False, False, False, False
    elif letter == ord('s'):
        s = True
        dead, f_v, b_b, b_v, f_b, o = False, False, False, False, False, False
    elif letter == ord('j'):
        b_b = True
        dead, f_v, s, b_v, f_b, o = False, False, False, False, False, False
    elif letter == ord('d'):
        dead = True
        b_b, f_v, s, b_v, f_b, o = False, False, False, False, False, False 
    elif letter == ord('g'):
        f_v = True
        b_b, dead, s, b_v, f_b, o = False, False, False, False, False, False
    elif letter == ord('h'):
        b_v = True
        b_b, f_v, s, f_b, o, dead = False, False, False, False, False, False
    elif letter == ord('o'):
        o = True
        b_b, f_v, s, b_v, f_b, dead = False, False, False, False, False, False
    elif letter == ord('w'):
        if fast:
            fast = False
        else:
            fast = True
    elif letter == ord('q'):
        dead, b_b, f_v, s, f_v, f_b, b_v, o = False, False, False, False, False, False, False, False
    elif letter == ord('p'):
        if pause:
            pause = False
        else:
            pause = True
    elif letter == ord('r'):
        remove = True




i = 0
while(video.isOpened()):
    isTrue, frame = video.read()
    i +=1
    cv.imshow('Video', frame)
    print(i)
    if pause:
        letter = cv.waitKey(0)
    else:
        if fast:
            letter = cv.waitKey(1)
        else:
            letter = cv.waitKey(70)
    saveImage(letter)
    if remove:
        if dead:
            strokes.get('Dead').pop()
            print('remove from D')
        elif f_v:
            strokes.get('ForehandV').pop()
            print('remove from FV')
        elif f_b:
            strokes.get('ForehandB').pop()
            print('remove from FB')
        elif b_b:
            strokes.get('BackhandB').pop()
            print('remove from BB')
        elif b_v:
            strokes.get('BackhandV').pop()
            print('remove from BV')
        elif s:
            strokes.get('Serve').pop()
            print('remove from S')
        elif o:
            strokes.get('OverheadV').pop()
            print('remove from O')
        remove = False
    elif dead:
        strokes.get('Dead').append(frame)
        print('D')
    elif f_v:
        strokes.get('ForehandV').append(frame)
        print('FV')
    elif f_b:
        strokes.get('ForehandB').append(frame)
        print('FB')
    elif b_b:
        strokes.get('BackhandB').append(frame)
        print('BB')
    elif b_v:
        strokes.get('BackhandV').append(frame)
        print('BV')
    elif s:
        strokes.get('Serve').append(frame)
        print('S')
    elif o:
        strokes.get('OverheadV').append(frame)
        print('O')

    else:
        print('Q')
        i-=1

    if letter == ord('c'):
        break
path = r'C:\Users\Study\Desktop\Research(non-git)\TrainingSet_1'

i = 40000
for stroke in strokes:
    for img in strokes.get(stroke):
        cv.imwrite(os.path.join(path, stroke, 'stroke{0}.jpg'.format(i)), img)
        i += 1

video.release()
cv.destroyAllWindows()
